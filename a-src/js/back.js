//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import anime  from "animejs"; 

//  ============================================================================================================
//  Back to top functionality
//  ============================================================================================================
window.addEventListener('DOMContentLoaded', function(){

    // Get the element that triggers the back to top functionality
    const back = document.querySelector('[data-back]');

    // When the user scrolls we add an event listener
    window.addEventListener('scroll', function(){
        // on scroll detect if we are half way down the page
        // get page height
        let windowHeight = window.innerHeight;
    
        // get 1% of the window height and multiply by 50
        let scrollValue = (windowHeight / 100) * 50;
    
        // Current y scroll
        let yOffset = window.pageYOffset;
    
        // This is when we will set our back to top button to show
        if(yOffset >= scrollValue){
            back.setAttribute('data-back','true');
        } else {
            back.removeAttribute('data-back','true');
        }
     });
    
    back.addEventListener('click',function(){
        
        // Using animejs to animate an object property
        // Set the object of the current window.pageYoss
        const backObject = {
            y: window.pageYOffset,
        }
        
        const an = anime({
            targets: backObject,
            y: 0,

            easing: 'easeInOutQuad',
            round: 1,
            duration: 300,
            update: function() {
                window.scroll(0, backObject.y);
            }
        });
    });
});
