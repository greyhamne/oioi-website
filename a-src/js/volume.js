//  ============================================================================================================
//  Create a function to toggle volume control on main video
//  ============================================================================================================

// Assign our toggle button to our data att
var toggleVol = document.querySelector('[data-toggle-volume]');
var video = document.querySelector('[data-video]');
var img = document.querySelector('[data-vol-img]');
var imagePath = "../assets/images/";

// Add an eventListener that changes the 
toggleVol.addEventListener('click',function(){
    // As this is an href lets prevent default behavior
    event.preventDefault();

    if(video.muted === true){
        // Toggle video volume status as well as its icon
        video.muted = false;
        img.src = imagePath + "volume-mute2.svg";
    } else {
        // Toggle video volume status as well as its icon
        video.muted = true;
        img.src = imagePath + "volume-high.svg";
    }
});

