//  ============================================================================================================
//  Dependencies
//  ============================================================================================================
import anime  from "animejs"; 

//  ============================================================================================================
//  Creating a time for the oioi intro
//  ============================================================================================================

window.addEventListener('DOMContentLoaded', function(){

    // Our logo, top and bottom halves of our intro
    const intro = document.querySelector("[data-intro]");
    const logoIntro = document.querySelector("[data-logo-intro]");
    const bottomIntro = document.querySelector("[data-bottom-intro]");
    const topIntro = document.querySelector("[data-top-intro]");

    // Bottom triangle animation
    const bottom = anime({
        targets: bottomIntro,
        translateX:-1000,
        translateY:1000,
        duration: 1000,
        delay: 1000,
        easing: "easeInQuad"
    });

    // Top right triangle
    const top = anime({
        targets: topIntro,
        translateX:1000,
        translateY:-1000,
        duration: 1000,
        delay: 1000,
        easing: "easeInQuad"
    });


    var logoTime = anime.timeline();

    logoTime
        .add({
            targets: logoIntro,
            top: "25%",
            easing: 'easeOutExpo',
        })
        .add({
            targets: logoIntro,
            top: "0",
            left: "0",
            width: "200",
            height: "300",
            opacity: "0",
            easing: 'easeOutExpo'
        })
        .add({
            targets: [bottomIntro, topIntro, logoIntro,intro], // reseting a lot of the div's that would cause overlay issues
            height: 0,
            width: 0,
        })
});