//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

    import gulp from "gulp";
	import gutil from "gulp-util";

	import path from "path";

	import through2 from "through2";
	import stream from "vinyl-source-stream";
	import buffer from "vinyl-buffer";

	import babelify from "babelify";
	import browserify from "browserify";

	import uglify from "gulp-uglify";
    import sourcemaps from "gulp-sourcemaps"
    
    // To be removed
    import browserSync from 'browser-sync'

//  ============================================================================================================
//  Browserify
//  ============================================================================================================

//  Lean error log
function LOG_ERROR(error) {
    if (error.codeFrame) {
        console.log("\n");
        console.log(error.codeFrame);
        console.log("\n");
        console.log(error.loc);
        console.log(error.filename);
        console.log(error.SyntaxError);
        console.log("\n");
    }
    else {
        console.log(error);
    }

    //  Important for Browserify
    this.emit("end");
}

// Compile Scripts
gulp.task("scripts", function compileScripts(complete){
  
    let transform = browserify({
        "debug": true,
        "paths": [
            "a-src/js"
        ],
    }).transform(babelify, {
        // "sourceMaps": (production) ? false : true,
        "presets": [
            ["env", {
                "targets": {
                    "browsers" : ["last 2 versions", "ie >= 10"]
                }
            }]
        ]
    });

    gulp.src([
        "a-src/js/*.js",

    ])
    .pipe(
        through2.obj(
            function write(file, enc, next) {
                transform.add(file.path);
                next();
            },
            function end(next) {

                transform.bundle()
                    .pipe(stream("bundle.js"))
                    .pipe(buffer())
                    //.pipe(sourcemaps.init({ loadMaps: true }))
                    .pipe(uglify())
                    // add sourcemaps
                    //.pipe(sourcemaps.write("./"))
                    .pipe(gulp.dest("a-dist/assets/js"))
                    .on("finish", function() {
                        complete();
                        browserSync.reload()
                    });


                next();

            }
        )
    );

});