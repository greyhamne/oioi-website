//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from "gulp";

// Used for grabbing the module name of an external library rather than the file path
import importer from "sass-module-importer"

import browserSync from "browser-sync";

// Path of our font file(s)
const videoSrcPath = 'a-src/video/*';

// This is where the font will be moved to dist
const videoDistPath  = 'a-dist/assets/video';

//  ============================================================================================================
//  Gulp Task for creating our main stylesheet
//  ============================================================================================================

gulp.task('video', function(){

    return gulp.src(videoSrcPath)

    .pipe(gulp.dest(videoDistPath))

    .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation
});
