//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from "gulp";

// Used for grabbing the module name of an external library rather than the file path
import importer from "sass-module-importer"

import browserSync from "browser-sync";

// Path of our font file(s)
const fontSrcPath = 'a-src/fonts/**.*';

// This is where the font will be moved to dist
const fontDistPath  = 'a-dist/assets/fonts';

//  ============================================================================================================
//  Gulp Task for creating our main stylesheet
//  ============================================================================================================

gulp.task('fonts', function(){

    return gulp.src(fontSrcPath)

    .pipe(gulp.dest(fontDistPath))

    .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation
});
