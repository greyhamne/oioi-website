//  ============================================================================================================
//  Dependencies
//  ============================================================================================================

import gulp from "gulp";

import imagemin from 'gulp-imagemin';

// Path of our images
const imgSrcPath = 'a-src/images/**/*';

// This is where the font will be moved to dist
const imgDistPath  = 'a-dist/assets/images';

//  ============================================================================================================
//  Gulp Task for compressing images
//  ============================================================================================================

gulp.task('images', () =>
    gulp.src(imgSrcPath)
        .pipe(imagemin())
        .pipe(gulp.dest(imgDistPath))
);